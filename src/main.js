var obj = {
  virus: null,
  computer: null,
  earth: null,
  male: null,
  female: null,
  bat: null,
  tree: null,
  shroom: null,
  alien: null
}
var angle = 0
var texts = {
  font: null,
  title: 'VIRUS',
  warning: 'WARNING!',
  infection: 'YOUR COMPUTER IS INFECTED',
  def: 'A virus is a submicroscopic infectious agent that replicates only inside the living cells of an organism. Viruses infect all types of life forms, from animals and plants to microorganisms, including bacteria and archaea. They expand uncontrollably, destroying their host.',
  animals: 'Every being on the planet instinctively develops a natural equilibrium with the surrounding environment, but humans do not.',
  comp: 'They rip down entire forests to build their homes, they destroy fragile ecosystems to support their needs. They completely wipe other species off the face of the planet. The only way they can survive is to spread to another area.',
  conc: 'There is another organism on this planet that follows the same pattern: a virus.',
  cure: 'Human beings are a disease, a cancer of that planet. They are a plague, and we are the cure.'
}
var states = {
  counter: 0
}

function preload() {
  texts.font = loadFont('ttf/synemono.ttf')
  obj.virus = loadModel('obj/virus.obj')
  obj.computer = loadModel('obj/computer.obj')
  obj.earth = loadModel('obj/earth.obj')
  obj.shroom = loadModel('obj/shroom.obj')
  obj.tree = loadModel('obj/tree.obj')
  obj.bat = loadModel('obj/bat.obj')
  obj.male = loadModel('obj/male.obj')
  obj.female = loadModel('obj/female.obj')
  obj.alien = loadModel('obj/alien.obj')
}

function setup() {
  createCanvas(640, 480, WEBGL)
}

function draw() {
  background(255)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  ambientLight(255, 0, 255)
  directionalLight(255, 255, 255, 0, 0, 1)

  textFont(texts.font)
  if (states.counter === 0) {
    fill(0)
    noStroke()
    textAlign(CENTER, CENTER)
    textSize(220)
    text(texts.title, 0, -25)

    push()
    translate(210, -175, 0)
    push()
    noStroke()
    specularMaterial(128, 255, 0, 0.75)
    scale(4)
    rotateY(-angle)
    model(obj.virus)
    pop()
    pop()

    push()
    translate(-200, 100, 0)
    push()
    noStroke()
    specularMaterial(0, 255, 0, 0.5)
    scale(2)
    rotateY(angle * 1.5)
    model(obj.virus)
    pop()
    pop()

    push()
    translate(100, 200, 0)
    push()
    noStroke()
    specularMaterial(128, 255, 255, 0.25)
    scale(10)
    rotateY(-angle * 2)
    model(obj.virus)
    pop()
    pop()
  } else if (states.counter === 1) {
    fill(255, 0, 0)
    textAlign(CENTER, CENTER)
    textSize(120)
    text(texts.warning, 0, -100)
    fill(0)
    textSize(60)
    text(texts.infection, 0, 80, 640, 480)

    push()
    translate(0, 0, 300)
    push()
    noStroke()
    specularMaterial(0, 255, 0, 0.3)
    scale(3)
    rotateY(-angle)
    rotateZ(-Math.PI)
    model(obj.computer)
    pop()
    pop()
  } else if (states.counter === 2) {
    fill(0)
    textAlign(LEFT, CENTER)
    textSize(30)
    text(texts.def, 0, 0, 560, 480)

    push()
    translate(0, 50, 100)
    push()
    noStroke()
    specularMaterial(0, 255, 0, 0.25)
    scale(4)
    rotateY(-angle)
    model(obj.virus)
    pop()
    pop()

    push()
    translate(230, 130, 50)
    push()
    noStroke()
    specularMaterial(128, 255, 0, 0.75)
    scale(3)
    rotateY(angle)
    model(obj.virus)
    pop()
    pop()

    push()
    translate(-300, -200, 50)
    push()
    noStroke()
    specularMaterial(0, 255, 128, 0.25)
    scale(5)
    rotateY(-angle * 0.3)
    model(obj.virus)
    pop()
    pop()
  } else if (states.counter === 3) {
    fill(0)
    textAlign(LEFT, CENTER)
    textSize(45)
    text(texts.animals, 0, 0, 560, 480)

    push()
    translate(0, 0, 50)
    push()
    noStroke()
    specularMaterial(0, 255, 128, 0.25)
    scale(5)
    rotateY(-angle * 0.3)
    model(obj.earth)
    pop()
    pop()

    push()
    translate(150, 0, 50)
    push()
    noStroke()
    specularMaterial(128, 255, 0, 0.5)
    scale(5)
    rotateZ(Math.PI)
    rotateX(angle * 0.3)
    rotateY(angle * 0.7)
    model(obj.shroom)
    pop()
    pop()

    push()
    translate(-150, 200, 50)
    push()
    noStroke()
    specularMaterial(0, 255, 0, 0.25)
    scale(50)
    rotateZ(Math.PI)
    rotateX(angle)
    rotateY(-angle * 2)
    model(obj.tree)
    pop()
    pop()

    push()
    translate(200, 120, 0)
    push()
    noStroke()
    specularMaterial(255, 0, 0, 0.5)
    scale(10)
    rotateZ(Math.PI)
    rotateY(-angle)
    model(obj.bat)
    pop()
    pop()
  } else if (states.counter === 4) {
    fill(0)
    textAlign(LEFT, CENTER)
    textSize(35)
    text(texts.comp, 0, 0, 560, 480)

    push()
    translate(100, 575, 100)
    push()
    noStroke()
    specularMaterial(0, 255, 128, 0.25)
    scale(10)
    rotateY(-angle * 0.3)
    rotateZ(Math.PI)
    model(obj.male)
    pop()
    pop()

    push()
    translate(-100, 575, 100)
    push()
    noStroke()
    specularMaterial(255, 255, 0, 0.3)
    scale(10)
    rotateY(-angle * 0.3)
    rotateZ(Math.PI)
    model(obj.female)
    pop()
    pop()
  } else if (states.counter === 5) {
    fill(0)
    textAlign(LEFT, CENTER)
    textSize(55)
    text(texts.conc, 0, 0, 560, 480)

    push()
    translate(150, 175, 200)
    push()
    noStroke()
    specularMaterial(128, 255, 128, 0.5)
    scale(8)
    rotateY(-angle * 0.3)
    model(obj.virus)
    pop()
    pop()

    push()
    translate(-100, -175, 50)
    push()
    noStroke()
    specularMaterial(0, 0, 128, 0.5)
    scale(2)
    rotateY(angle * 0.6)
    model(obj.virus)
    pop()
    pop()
  } else if (states.counter === 6) {
    fill(0)
    textAlign(LEFT, CENTER)
    textSize(50)
    text(texts.cure, 0, 0, 560, 480)

    push()
    translate(-150, -100, 200)
    push()
    noStroke()
    specularMaterial(0, 128, 128, 0.25)
    scale(7)
    rotateY(-angle * 0.3)
    model(obj.virus)
    pop()
    pop()

    push()
    translate(150, 750, 50)
    push()
    noStroke()
    specularMaterial(0, 255, 0, 0.25)
    scale(100)
    rotateZ(Math.PI)
    rotateY(angle * 0.9)
    model(obj.alien)
    pop()
    pop()
  }
  noFill()
  stroke(0)
  strokeWeight(32)
  rect(0, 0, 640, 480)

  angle += 0.0025
}

function mousePressed() {
  if (states.counter < 6) {
    states.counter++
  } else {
    states.counter = 0
  }
}

function windowResized() {
  createCanvas(640, 480, WEBGL)
}
